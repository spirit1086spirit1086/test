<?php

namespace App;
use DOMDocument;
use http\Exception\RuntimeException;

class Sign
{
    protected $file,$password,$key_path,$signature,$private_key,$public_key,$doc_path;

    function __construct($file,$password,$key_path='./keys/',$doc_path='./document/')
    {
        $this->file = $file;
        $this->password = $password;
        $this->key_path = $key_path;
        $this->doc_path = $doc_path;
        $this->private_key = 'private_key.pem';
        $this->public_key = 'public_key.pem';
    }

    public function getXml()
    {
        return simplexml_load_file($this->doc_path.$this->file);
    }


    public function createKeys()
    {
        $new_key_pair = openssl_pkey_new(array(
            "private_key_bits" => 2048,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ));
        openssl_pkey_export($new_key_pair, $sign_private_key,$this->password);

        $details = openssl_pkey_get_details($new_key_pair);
        $public_key_pem = $details['key'];

        $this->saveKeys($sign_private_key,$public_key_pem);
    }

    private function saveKeys($private_key,$public_key)
    {
        file_put_contents($this->key_path.$this->private_key, $private_key);
        file_put_contents($this->key_path.$this->public_key, $public_key);
    }

    private function getPrivateKey()
    {
        $sign_private_key = openssl_pkey_get_private(file_get_contents($this->key_path.$this->private_key),$this->password);

        if ($sign_private_key === false) {
            throw new \Exception(openssl_error_string());
        }

        return $sign_private_key;
    }

    public function getPublicKey()
    {
        $sign_public_key = openssl_pkey_get_public(file_get_contents($this->key_path.$this->public_key));

        if ($sign_public_key === false) {
            throw new \Exception(openssl_error_string());
        }

        return $sign_public_key;
    }

    public function calculateSign($data)
    {
        $sign_private_key = $this->getPrivateKey();
        $status = openssl_sign($data, $signature, $sign_private_key, OPENSSL_ALGO_SHA256);
        if (!$status) {
            throw new \Exception('Computing of the signature failed');
        }
        $this->signXml($signature,$data);
    }


    public function signXml($signature,$data)
    {
        $xml = new DOMDocument();
        $xml->load($this->doc_path.$this->file);
        $xml->preserveWhiteSpace = true;
        $xml->formatOutput = true;

        $signature_value = base64_encode($signature);

        $digest_value = base64_encode(hash('sha512', $data, true));

        $signatureElement = $xml->createElement('Signature');
        $signatureElement->setAttribute('xmlns', 'http://www.w3.org/2000/09/xmldsig#');

        $signedInfoElement = $xml->createElement('SignedInfo');
        $signatureElement->appendChild($signedInfoElement);

        $canonicalizationMethodElement = $xml->createElement('CanonicalizationMethod');
        $canonicalizationMethodElement->setAttribute('Algorithm', 'http://www.w3.org/TR/2001/REC-xml-c14n-20010315');
        $signedInfoElement->appendChild($canonicalizationMethodElement);

        $signatureMethodElement = $xml->createElement('SignatureMethod');
        $signatureMethodElement->setAttribute('Algorithm', 'http://www.w3.org/2000/09/xmldsig#rsa-sha512');
        $signedInfoElement->appendChild($signatureMethodElement);

        $referenceElement = $xml->createElement('Reference');
        $referenceElement->setAttribute('URI', '');
        $signedInfoElement->appendChild($referenceElement);

        $transformsElement = $xml->createElement('Transforms');
        $referenceElement->appendChild($transformsElement);

        $transformElement = $xml->createElement('Transform');
        $transformElement->setAttribute('Algorithm', 'http://www.w3.org/2000/09/xmldsig#enveloped-signature');
        $transformsElement->appendChild($transformElement);

        $digestMethodElement = $xml->createElement('DigestMethod');
        $digestMethodElement->setAttribute('Algorithm', 'http://www.w3.org/2000/09/xmldsig#sha512');
        $referenceElement->appendChild($digestMethodElement);

        $digestValueElement = $xml->createElement('DigestValue', $digest_value);
        $referenceElement->appendChild($digestValueElement);

        $signatureValueElement = $xml->createElement('SignatureValue', $signature_value);
        $signatureElement->appendChild($signatureValueElement);

        $xml->documentElement->appendChild($signatureElement);

        file_put_contents($this->doc_path.$this->file, $xml->saveXML());
    }

    public function signVerify($sign_public_key)
    {
        $signedExample = simplexml_load_file($this->doc_path.$this->file);
        $signature_value = $signedExample->Signature->SignatureValue;
        $signature_decode = base64_decode($signature_value);

        $verify = openssl_verify($signedExample, $signature_decode, $sign_public_key,  "sha256WithRSAEncryption");
        if ($verify == 1) {
            $result = "Подпись корректная";
        } elseif ($verify == 0) {
            $result =  "Подпись некорректная";
        } else {
            $result =  "Произошла какая-то ошибка, печаль :(";
        }

        return $result;
    }
}