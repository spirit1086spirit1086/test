<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6adae821badc19dd9ce5c32488026417
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6adae821badc19dd9ce5c32488026417::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6adae821badc19dd9ce5c32488026417::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit6adae821badc19dd9ce5c32488026417::$classMap;

        }, null, ClassLoader::class);
    }
}
