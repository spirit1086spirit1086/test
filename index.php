<?php
require_once __DIR__.'/vendor/autoload.php';
use App\Sign;

$filename = 'doc.xml';
$password = 'mypass';

/**
 * Передаем  название файла которого хотим подписать и пароль для закрытого ключа
*/
$sign = new Sign($filename,$password);

/**
 * генерируем ключи
 */
$sign->createKeys();

/**
 * получаем документ
 */
$data = $sign->getXml();

/**
 * вычисляем сигнатуру документа и добавляем попись
 */
$sign->calculateSign($data);

/**
 * получаем открытый ключ
 */
$public_key = $sign->getPublicKey();

/**
 * проверяем подписанный документ
 */
$result = $sign->signVerify($public_key);
echo $result;
?>